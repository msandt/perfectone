<?php

require('protected/vendor/autoload.php');
require('protected/vendor/password.php');
require('protected/vendor/marcovtwout/yii-environment/Environment.php');

// set environment
$env = new \marcovtwout\YiiEnvironment\Environment;

// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG', $env->yiiDebug);
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', $env->yiiTraceLevel);

require_once($env->yiiPath);
Yii::createWebApplication($env->configWeb)->run();
