# Perfectone API & Web Portal

## Completed Work

* Created Migrations for Getting Users Table Up and Running
* Implemented bcrypt for auth & storing passwords server side
* Readme creation
* Prettyfied URLS

## TODO

* ~~API key based authentication~~
* User related API methods Create/Login/Update/Delete
* ~~User weight, weight ranges API~~
* ~~User subscription API~~
* ~~Update program day API~~
* ~~Add favorites API~~
* ~~Remove favorites API~~

* HTTPS Config for AWS
* Documentation of API
* Creation of Migrations as driven by app
* Unit Testing for Models & API

## BEFORE YOU START

Please read the following [article](http://nvie.com/posts/a-successful-git-branching-model/) on the workflow to be used. In coming implementations the master branch will automatically be pushed to production. As such, the master branch is for PRODUCTION READY CODE ONLY

Please create a new branch for your feature, hotfix, or bug fix based off the dev branch. After a specified amount of time the dev branch will be rolled into a release candidate and tested. Afterwards it will be merged with master for auto deployment to the server. 

#### Example Git Workflow

##### Clone the dev branch
	
	$ git clone repo
	$ git checkout dev

##### Create a Feature Branch

	$ git checkout -b newfeature

##### Make some changes and commit

	$ touch newfile.txt
	$ git add newfile.txt
	$ git commit -m "added a file"
	$ git push

##### After testing merge with dev branch

	$ git checkout dev
	$ git merge newfeature

##### After testing integration merge with release

	$ git checkout release
	$ git merge dev

These changes will be automatically available on the [server](http://177.71.255.246/backend/perfectone-staging)


## Running the Project

This project uses [Composer](https://getcomposer.org/) for dependency management. Please install it first

##### Clone the repo

	git clone https://msandt@bitbucket.org/msandt/perfectone.git

##### Install the dependencies

	$ cd /path/to/the/repo
	$ composer install (or composer update if you've already run composer install)

##### Make sure you have a (L)AMP stack or similar set up

[Setting up LAMP](https://www.digitalocean.com/community/tutorials/how-to-install-linux-apache-mysql-php-lamp-stack-on-ubuntu)

##### Create databases 'perfectone' and 'perfectone_test' on your local machine

##### Update the DB connection strings in config/mode_development.php to reflect your current settings

##### Open mode.php.example and modify the contents to read DEVELOPMENT

##### Save mode.php.example as mode.php in the same directory

##### Run the Migrations
	
	$ cd /path/to/the/repo/protected
	$ ./yiic migrate up

##### Symlink the codebase and /var/www (Linux)

	$ ln -s /var/www /path/to/repo

##### navigate to localhost/perfectone to see the UI

	localhost/perfectone

## Running the Tests

Some tests are not fully finished as of yet and will fail. However follow the instructions to use phpunit

##### cd into the test directory

	$ cd/path/to/repo/protected/tests

##### to run ALL tests

	$ ../../vendor/bin/./phpunit .

##### to run ONE test follow the phpunit command with the file you'd like to test i.e.

	$ ../../vendor/bin/./phpunit unit/UsersTest


## API Documentation


~~Be sure to submit all POST requests with Content-Type: x-www-form-urlencoded~~
Supported Content-Type headers for POST & PUT

	application/json
	x-www-form-urlencoded
	

### Create User API
	
	POST - localhost/perfectone/api/users

	Payload

		email - required
		password - required
		facebook_id
		first_name - required
		surname - required
		gender - (M or F) required
		subscribed - (1 or 0) required
		height
		current_day
		current_weight
		plan_type



### Login Api
	
	POST - localhost/perfectone/api/login

	Payload 

		email
		password

This API will return an API token upon successful authentication. The token is valid for 30 days or the next successful login. Append the token to subsequent API requests to remain authenticated. 


### List Users API

	GET - localhost/perfectone/api/users?token=<api_token>

### Get User API

	GET - localhost/perfectone/api/users/<id>?token=<api_token>

### Update User API

	PUT - localhost/perfectone/api/users/<id>?token=<api_token>

	Payload

		first_name 
		surname 
		gender 
		subscribed 
		height
		current_day
		current_weight
		plan_type

	Note: password, email, facebook_id, api_token and token_expires will not be processed if included
	You can use as many or as few parameters for this request as you like

### Update Current Day API
	
	Updating the current day can be accomplished by using the call above, I have however implemented a standalone API method for this if you wish to use it. 

	PUT - localhost/perfectone/api/users/<id>/updateDay?token=<api_token>

	Payload

		current_day - between 1 - 40

### Add user weight API

	POST - localhost/perfectone/api/users/<id>/weight?token=<api_token>

	Payload

		weight - required
		date - required (YYYY-MM-DD)

### Get weights in range API

	GET - localhost/perfectone/api/users/<id>/weight?token=<api_token>&start_date=<start_date>&end_date=<end_date>

	I.E.

	GET - localhost/perfectone/api/users/1/weight?token=b640b5c67c8abc1e313be498d169335f&start_date=2014-7-8 0:0:0&end_date=2014-7-8 23:59:59

	Dates are in format YYYY-MM-DD -- be sure that you include a leading 0 in both MM and DD for numbers < 10

### Subscribe User API

	Updating the subscription status can be accomplished using the overarching User PUT method, but I have implemented one for only setting subscribed as well

	PUT - localhost/perfectone/api/users/<id>/subscribe?token=<api_token>

	Payload

		subscribed - required (0 or 1)

### Get Favorites

	GET - localhost/perfectone/api/users/<id>/favorites?token=<api_token>

### Add Favorites

	POST - localhost/perfectone/api/users/<id>/favorites?token=<api_token>

	Payload

		program_id - this wont have any values yet, but the API is implemented

### Delete Favorites

	DELETE - localhost/perfectone/api/users/<id>/favorites/<favorite_id>?token=<api_token>

