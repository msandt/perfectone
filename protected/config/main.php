<?php

/**
 * Main configuration.
 * All properties can be overridden in mode_<mode>.php files
 */

return array(

    // Set yiiPath
    'yiiPath' => __DIR__ . '/../vendor/square1-io/yii-framework/yii.php',
    'yiicPath' => __DIR__ . '/../vendor/square1-io/yii-framework/yiic.php',
    'yiitPath' => __DIR__ . '/../vendor/square1-io/yii-framework/yiit.php',

    // Set YII_DEBUG and YII_TRACE_LEVEL flags
    'yiiDebug' => true,
    'yiiTraceLevel' => 0,

    // This is the main Web application configuration. Any writable
    // CWebApplication properties can be configured here.
    'configWeb' => array(
    
        'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
        'name' => 'PerfecTone',
        
        // Aliases
        'aliases' => array(
            // uncomment the following to define a path alias
            //'local' => 'path/to/local-folder'
        ),

        // Preloading 'log' component
        'preload' => array('log'),

        // Autoloading model and component classes
        'import' => array(
            'application.models.*',
			'application.components.*',
			'application.vendor.*',
        ),
        
        // Application components
        'components' => array(
        
            'user' => array(
                // enable cookie-based authentication
                'allowAutoLogin' => true,
            ),
            
           'urlManager'=>array(
				'urlFormat'=>'path',
				'showScriptName'=>false,
				'rules'=>array(
					//RESTFUL API ROUTES
					array('api/list', 'pattern'=>'api/<model:\w+>', 'verb'=>'GET'), //List all of a model
					array('api/list', 'pattern'=>'api/<model:\w+>/<id:\d+>/<relation:\w+>', 'verb'=>'GET'),
					array('api/view', 'pattern'=>'api/<model:\w+>/<id:\d+>', 'verb'=>'GET'), //Get a particular model by id
					array('api/update', 'pattern'=>'api/<model:\w+>/<id:\d+>', 'verb'=>'PUT'),
					array('api/update', 'pattern'=>'api/<model:\w+>/<id:\d+>/<relation:\w+>', 'verb'=>'PUT'),
					array('api/delete', 'pattern'=>'api/<model:\w+>/<id:\d+>', 'verb'=>'DELETE'),
					array('api/delete',	'pattern'=>'api/<model:\w+>/<id:\d+>/<relation:\w+>/<rel_id:\d+>', 'verb'=>'DELETE'),
					array('api/create', 'pattern'=>'api/<model:\w+>', 'verb'=>'POST'),
					array('api/create', 'pattern'=>'api/<model:\w+>/<id:\d+>/<relation:\w+>', 'verb'=>'POST'),
					//array('api/create',	'pattern'=>'api/<model:\w+>/<id:\d+>/<relation:\w+>/<rel_id:\d+>', 'verb'=>'POST'),
					'<controller:\w+>/<id:\d+>'=>'<controller>/view',
					'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
					'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
				),
			),

            // Database
            'db' => array(
                'connectionString' => '', //override in config/mode_<mode>.php
                'username' => '', //override in config/mode_<mode>.php
                'password' => '', //override in config/mode_<mode>.php
                'charset' => 'utf8',
            ),

            // Error handler
            'errorHandler'=>array(
                // use 'site/error' action to display errors
                'errorAction'=>'site/error',
            ),

        ),

        // application-level parameters that can be accessed
        // using Yii::app()->params['paramName']
        'params'=>array(
            // this is used in contact page
            'adminEmail'=>'webmaster@example.com',
        ),

    ),

    // This is the Console application configuration. Any writable
    // CConsoleApplication properties can be configured here.
    // Leave array empty if not used.
    // Use value 'inherit' to copy from generated configWeb.
    'configConsole' => array(
    
        'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
        'name' => 'My Console Application',
        
        // Aliases
        'aliases' => 'inherit',

        // Preloading 'log' component
        'preload' => array('log'),

        // Autoloading model and component classes
        'import'=>'inherit',

        // Application componentshome
        'components'=>array(

            // Database
            'db'=>'inherit',

            // Application Log
            'log' => array(
                'class' => 'CLogRouter',
                'routes' => array(
                    // Save log messages on file
                    array(
                        'class' => 'CFileLogRoute',
                        'levels' => 'error, warning, trace, info',
                    ),
                ),
            ),

        ),

    ),

);