<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'My Web Application',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.vendor.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'root',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
	),

	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
		// uncomment the following to enable URLs in path-format
		
		'urlManager'=>array(
			'urlFormat'=>'path',
			'showScriptName'=>false,
			'rules'=>array(
				//RESTFUL API ROUTES
				array('api/list', 'pattern'=>'api/<model:\w+>', 'verb'=>'GET'), //List all of a model
				array('api/list', 'pattern'=>'api/<model:\w+>/<id:\d+>/<relation:\w+>', 'verb'=>'GET'),
				array('api/view', 'pattern'=>'api/<model:\w+>/<id:\d+>', 'verb'=>'GET'), //Get a particular model by id
				array('api/update', 'pattern'=>'api/<model:\w+>/<id:\d+>', 'verb'=>'PUT'),
				array('api/update', 'pattern'=>'api/<model:\w+>/<id:\d+>/<relation:\w+>', 'verb'=>'PUT'),
				array('api/delete', 'pattern'=>'api/<model:\w+>/<id:\d+>', 'verb'=>'DELETE'),
				array('api/delete',	'pattern'=>'api/<model:\w+>/<id:\d+>/<relation:\w+>/<rel_id:\d+>', 'verb'=>'DELETE'),
        		array('api/create', 'pattern'=>'api/<model:\w+>', 'verb'=>'POST'),
        		array('api/create', 'pattern'=>'api/<model:\w+>/<id:\d+>/<relation:\w+>', 'verb'=>'POST'),
        		//array('api/create',	'pattern'=>'api/<model:\w+>/<id:\d+>/<relation:\w+>/<rel_id:\d+>', 'verb'=>'POST'),
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		
		// 'db'=>array(
		// 	'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		// ),
		// uncomment the following to use a MySQL database
		

		//This will need to be changed for move to deployment -- adjust username/password to you local box
		//DEV
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=perfectone;port=3306',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => 'root',
			'charset' => 'utf8',
		),
		
		// 'db'=>array(
		// 	'connectionString' => 'mysql:host=' . ($_SERVER['HTTP_HOST'] == 'localhost' ? '177.71.255.246' : 'localhost') . ';dbname=perfectone;port=3306',
		// 	'username' => 'root',
		// 	'password' => 'g3k1g4ng4',
		// 	'charset' => 'utf8',
		// ),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
		//fixture for testing purposes
		'fixture'=>array(
			'class'=>'system.test.CDbFixtureManager',
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
	),
);