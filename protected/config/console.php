<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'My Console Application',

	// preloading 'log' component
	'preload'=>array('log'),

	// application components
	'components'=>array(
		// 'db'=>array(
		// 	'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		// ),
		// uncomment the following to use a MySQL database
		

		//This will need to be changed for move to deployment -- adjust username/password to you local box
		//DEV
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=perfectone;port=3306',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => 'root',
			'charset' => 'utf8',
		),
		//PROD
		// 'db'=>array(
		// 	'connectionString' => 'mysql:host=' . ($_SERVER['HTTP_HOST'] == 'localhost' ? '177.71.255.246' : 'localhost') . ';dbname=perfectone;port=3306',
		// 	'username' => 'root',
		// 	'password' => 'g3k1g4ng4',
		// 	'charset' => 'utf8',
		// ),
		
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
			),
		),
	),
);