<?php

class UsersTest extends CDbTestCase{

	public $fixtures = array(
		'users'=>'Users',
	);

	public function testCreate(){

		//First Assert We Cannot Create an Empty Record
		$user = new Users;
		$this->assertFalse($user->save());

		//With some but not min set of attr
		$user->setAttributes(array(
			'email'=>'newuser@email.com',
			'first_name'=>'New',
			'surname'=>'User',
		));
		$this->assertFalse($user->save());

		//With min set of attr
		$user->setAttributes(array(
			'email'=>'newuser@email.com',
			'first_name'=>'New',
			'surname'=>'User',
			'gender'=>'M'
		));
		$this->assertTrue($user->save());

		//check the relations -- make sure theyre empty
		$this->assertEquals(array(),$user->usersFavoritesRelations);
		$this->assertEquals(array(),$user->usersWeightRelations);


	}
}