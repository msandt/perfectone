<?php
	return array(
		'user1'=>array(
			'email'=>'test@email.com',
			'password'=>'$2y$10$OtptFObXArK8bPfMmart5Ox78B4vcx7Jf7PX/XIWsezsRxO5dGjd2',
			'first_name'=>'Test',
			'surname'=>'User',
			'gender'=>'M',
		),
		'user2'=>array(
			'email'=>'test2@email.com',
			'facebook_id'=>'123',
			'first_name'=>'Test',
			'surname'=>'User2',
			'gender'=>'F',
		),
	);