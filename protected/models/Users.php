<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property integer $id
 * @property string $email
 * @property string $password
 * @property string $facebook_id
 * @property string $first_name
 * @property string $surname
 * @property string $gender
 * @property integer $subscribed
 * @property string $height
 * @property integer $current_day
 * @property integer $current_weight
 * @property integer $plan_type
 * @property string $api_token
 * @property string $token_expires
 *
 * The following are the available model relations:
 * @property UsersWeightRelation[] $usersWeightRelations
 * @property UsersFavoritesRelation[] $usersFavoritesRelations
 */
class Users extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'users';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('email, first_name, surname, gender', 'required'),
			array('subscribed, current_weight, plan_type', 'numerical', 'integerOnly'=>true),
			array('current_day','numerical','integerOnly'=>true,'min'=>1,'max'=>40,'tooSmall'=>'Day must be >= 1','tooBig'=>'Day must be <= 40'),
			array('email, api_token', 'length', 'max'=>255),
			array('password', 'length', 'max'=>60),
			array('facebook_id', 'length', 'max'=>128),
			array('first_name, surname', 'length', 'max'=>32),
			array('gender', 'length', 'max'=>1),
			array('gender','in','range'=>array('M','F')),
			array('subscribed','in','range'=>array(0,1)),
			array('height', 'length', 'max'=>4),
			array('token_expires', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, email, password, facebook_id, first_name, surname, gender, subscribed, height, current_day, current_weight, plan_type, api_token, token_expires', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'usersFavoritesRelations' => array(self::HAS_MANY, 'UsersFavoritesRelation', 'user_id'),
			'usersWeightRelations' => array(self::HAS_MANY, 'UsersWeightRelation', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'email' => 'Email',
			'password' => 'Password',
			'facebook_id' => 'Facebook',
			'first_name' => 'First Name',
			'surname' => 'Surname',
			'gender' => 'Gender',
			'subscribed' => 'Subscribed',
			'height' => 'Height',
			'current_day' => 'Current Day',
			'current_weight' => 'Current Weight',
			'plan_type' => 'Plan Type',
			'api_token' => 'Api Token',
			'token_expires' => 'Token Expires',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('facebook_id',$this->facebook_id,true);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('surname',$this->surname,true);
		$criteria->compare('gender',$this->gender,true);
		$criteria->compare('subscribed',$this->subscribed);
		$criteria->compare('height',$this->height,true);
		$criteria->compare('current_day',$this->current_day);
		$criteria->compare('current_weight',$this->current_weight);
		$criteria->compare('plan_type',$this->plan_type);
		$criteria->compare('api_token',$this->api_token,true);
		$criteria->compare('token_expires',$this->token_expires,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Users the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
