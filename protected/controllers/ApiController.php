<?php
/* TODOS
	Status codes for User Login/Create
	Generate token on creatin
	Filter passwords
	Status codes for POST Weight
	Status codes for Search Weight
	Status codes for Subscribe User
	Status codes for GET/PUT User
	Status codes for update day
	Status codes for add favorites
	Status codes for delete favorites
*/
class ApiController extends Controller{

	Const APP_ID = 'APPID';

	private $format = 'json';

	public function filters(){
		return array();
	}

	public function actionList(){
		switch($_GET['model']){
			case 'users':
				if(isset($_GET['relation']) && $_GET['relation'] == 'favorites')
					$this->_getFavorites();
				elseif(isset($_GET['relation']) && $_GET['relation'] == 'weight')
					$this->_getWeightBetweenDates();
				else
					$this->_listUsers();
				break;
			default:
				$msg = sprintf('Not implemented for model %s', $_GET['model']);
				$this->_sendResponse(501, CJSON::encode(array('Message'=>$msg)), 'application/json' );
				Yii::app()->end();

		}
		$msg = sprintf('No items where found for model %s', $_GET['model']);
		$this->_sendResponse(200, CJSON::encode(array('Message'=>$msg)), 'application/json');
	}

	public function actionView(){
		switch($_GET['model']){
			case 'users':
				$this->_getUser();
				break;
			default:
				$msg = sprintf('Not implemented for model %s', $_GET['model']);
				$this->_sendResponse(501, CJSON::encode(array('Message'=>$msg)), 'application/json' );
				Yii::app()->end();
		}
		$msg = sprintf('No items where found for model %s', $_GET['relation']);
		$this->_sendResponse(200, CJSON::encode(array('Message'=>$msg)), 'application/json');
	}

	public function actionCreate(){

		if(isset($_SERVER['CONTENT_TYPE']) && $_SERVER['CONTENT_TYPE'] === 'application/json'){
			$json = file_get_contents('php://input');
			parse_str($json,$_POST);
			// $this->_sendResponse(200, CJSON::encode($_POST), 'application/json');
		}
		switch($_GET['model']){
			case 'login':
				if(isset($_POST['email']) && isset($_POST['password']))
					$this->_login($_POST['email'],$_POST['password']);
				elseif(isset($_POST['email']) && isset($_POST['facebook_id']))
					$this->_loginFB($_POST['email'],$_POST['facebook_id']);
				break;
			case 'users':
				if(isset($_GET['relation']) && $_GET['relation'] == 'weight')
					$this->_createUserWeightRelation();
				elseif(isset($_GET['relation']) && $_GET['relation'] == 'favorites')
					$this->_createUserFavoriteRelation();
				else
					$this->_createUsers();
				break;
			default:
				$msg = sprintf('Not implemented for model %s', $_GET['model']);
				$this->_sendResponse(501, CJSON::encode(array('Message'=>$msg)), 'application/json' );
            	Yii::app()->end();
		}
		$msg = sprintf('No items where found for model %s', $_GET['model']);
		$this->_sendResponse(200, CJSON::encode(array('Message'=>$msg)), 'application/json');
	}

	public function actionUpdate(){

		//put the input variables in $_PUT
		$json = file_get_contents('php://input');
		parse_str($json,$_PUT);

		// $this->_sendResponse(200, CJSON::encode($_PUT),'application/json');


		switch($_GET['model']){
			case 'users':
				if(isset($_GET['relation']) && $_GET['relation'] == 'updateDay')
					$this->_updateDay($_PUT);
				elseif (isset($_GET['relation']) && $_GET['relation'] == 'subscribe')
					$this->_subscribeUser($_PUT);
				else
					$this->_updateUser($_PUT);
				break;
			default:
				$msg = sprintf('Not implemented for model %s', $_GET['model']);
				$this->_sendResponse(501, CJSON::encode(array('Message'=>$msg)), 'application/json' );
				Yii::app()->end();

		}
		$msg = sprintf('No items where found for model %s', $_GET['relation']);
		$this->_sendResponse(200, CJSON::encode(array('Message'=>$msg)), 'application/json');
	}

	public function actionDelete(){
		switch($_GET['model']){
			case 'users':
				if(isset($_GET['relation']) && $_GET['relation'] == 'favorites')
					$this->_deleteFavorite();
				break;
			default:
				$msg = sprintf('Not implemented for model %s', $_GET['model']);
				$this->_sendResponse(501, CJSON::encode(array('Message'=>$msg)), 'application/json' );
            	Yii::app()->end();
		}
		$msg = sprintf('No items where found for model %s', $_GET['model']);
		$this->_sendResponse(200, CJSON::encode(array('Message'=>$msg)), 'application/json');
	}

	private function _sendResponse($status = 200, $body = '', $content_type = 'text/plain')
	{
		// set the status
		$status_header = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);
		header($status_header);
		// and the content type
		header('Content-type: ' . $content_type);

		// pages with body are easy
		if($body != '')
		{
			// send the body
			echo $body;
		}
		// we need to create the body if none is passed
		else
		{
			// create some body messages
			$message = '';

			// this is purely optional, but makes the pages a little nicer to read
			// for your users.  Since you won't likely send a lot of different status codes,
			// this also shouldn't be too ponderous to maintain
			switch($status)
			{
				case 401:
					$message = 'You must be authorized to view this page.';
					break;
				case 404:
					$message = 'The requested URL ' . $_SERVER['REQUEST_URI'] . ' was not found.';
					break;
				case 500:
					$message = 'The server encountered an error processing your request.';
					break;
				case 501:
					$message = 'The requested method is not implemented.';
					break;
			}

			// servers don't always have a signature turned on
			// (this is an apache directive "ServerSignature On")
			$signature = ($_SERVER['SERVER_SIGNATURE'] == '') ? $_SERVER['SERVER_SOFTWARE'] . ' Server at ' . $_SERVER['SERVER_NAME'] . ' Port ' . $_SERVER['SERVER_PORT'] : $_SERVER['SERVER_SIGNATURE'];

			// this should be templated in a real-world solution
			$body = '
			<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
			<html>
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
			<title>' . $status . ' ' . $this->_getStatusCodeMessage($status) . '</title>
			</head>
			<body>
			<h1>' . $this->_getStatusCodeMessage($status) . '</h1>
			<p>' . $message . '</p>
			<hr />
			<address>' . $signature . '</address>
			</body>
			</html>';

			echo $body;
		}
		Yii::app()->end();
	}

	private function _getStatusCodeMessage($status)
	{
		// these could be stored in a .ini file and loaded
		// via parse_ini_file()... however, this will suffice
		// for an example
		$codes = Array(
				200 => 'OK',
				400 => 'Bad Request',
				401 => 'Unauthorized',
				402 => 'Payment Required',
				403 => 'Forbidden',
				404 => 'Not Found',
				500 => 'Internal Server Error',
				501 => 'Not Implemented',
		);
		return (isset($codes[$status])) ? $codes[$status] : '';
	}

	private function _login($email,$password){

		$record = Users::model()->findByAttributes(array('email' => $email));
		$error_json = array('Code'=>1,'Message'=>'Incorrect Username/Password');

		if($record == null){//if the user doesnt exist
			$this->_sendResponse(401, CSJON::encode($error_json), 'application/json');
			Yii::app()->end();
		}elseif(password_verify($password,$record->password) == false){//if the hash verification fails
			$this->_sendResponse(401, CSJON::encode($error_json), 'application/json');
			Yii::app()->end();
		}else{
			//username and password are correct
			//generate an API token, expires in 30 days
			$token = bin2hex(openssl_random_pseudo_bytes(16));

			//Save the expiration date for 30 days ahead in UTC time
			$record->api_token = $token;
			$now = new DateTime(null, new DateTimeZone('UTC'));
			$now->modify("+1 month");
			$record->token_expires = $now->format('Y/m/d H:i:s');

			if($record->save()){
				$response = array('Code'=>0,'api_token'=>$token);
				$this->_sendResponse(200, CJSON::encode($response), 'application/json');
				Yii::app()->end();
			}else{
				$json = array('Message'=>"Record could not be saved, please try again");
				$this->_sendResponse(500, CJSON::encode($json), 'application/json');
				Yii::app()->end();
			}
		}
	}

	private function _loginFB($email,$facebook_id){
		$record = Users::model()->findByAttributes(array('email'=>$email));
		$error_json = array('Code'=>1,'Message'=>'Incorrect Username/Password');

		if($record == null){//if the user doesnt exist
			$this->_sendResponse(401, CSJON::encode($error_json), 'application/json');
			Yii::app()->end();
		}elseif($facebook_id !== $record->facebook_id){
			$this->_sendResponse(401, CSJON::encode($error_json), 'application/json');
		}else{
			//username and password are correct
			//generate an API token, expires in 30 days
			$token = bin2hex(openssl_random_pseudo_bytes(16));

			//Save the expiration date for 30 days ahead in UTC time
			$record->api_token = $token;
			$now = new DateTime(null, new DateTimeZone('UTC'));
			$now->modify("+1 month");
			$record->token_expires = $now->format('Y/m/d H:i:s');

			if($record->save()){
				$response = array('Code'=>0,'api_token'=>$token);
				$this->_sendResponse(200, CJSON::encode($response), 'application/json');
				Yii::app()->end();
			}else{
				$json = array('Message'=>"Record could not be saved, please try again");
				$this->_sendResponse(500, CJSON::encode($json), 'application/json');
				Yii::app()->end();
			}
		}
	}

	private function _authenticate(){
		//check that the token provided is valid && that it has not expired
		$token = $_GET['token'];
		$user = Users::model()->findByAttributes(array('api_token' => $token));

		if($user != null){
			$expiration = new DateTime();
			$expiration = DateTime::createFromFormat('Y-m-d H:i:s', $user->token_expires);
			// echo date_diff($expiration,$currentTime);
			$currentTime = new DateTime(null, new DateTimeZone('UTC'));
			//$currentTime = DateTime::createFromFormat('Y/m/d H:i:s',$currentTime->format('Y/m/d H:i:s'));
			//if the current time is strictly less than the expiration, we're good
			if($currentTime < $expiration){
				return true;
			}else{
				//otherwise stop execution and inform the user their token is bad
				$json = array('Message'=>'Token expired, please login in again to generate a new one');
				$this->_sendResponse(401,CJSON::encode($json),'application/json');
				return false;
			}
		}else{
			$json = array('Message'=>'No user exists for this token!');
			$this->_sendResponse(404,CJSON::encode($json),'application/json');
			return false;
		}

	}

	private function _createUsers(){
		
		//Make sure we're not duplicating an email
		if(isset($_POST['email'])){
			$record = Users::model()->findByAttributes(array('email' => $_POST['email']));
		}else{
			$this->_sendResponse(400,
				CJSON::encode(
					array('Message'=>'Please include an email in your request')
				),
				'application/json');
		}

		if($record != null){
			//we need to check and see if theyve provided the facebook login cred & join them
			if($record->password !== null && isset($_POST['facebook_id'])){
				$record->facebook_id = $_POST['facebook_id'];
			}elseif($record->facebook_id != null && isset($_GET['password'])){//check to see if fb acct exists
				$hash = password_hash($_POST['password'],PASSWORD_BCRYPT);
				$record->password = $hash;
			}else{
				$json = array('Code'=>'1','Message'=>"This email is in use, please provide FB ID or Password to merge");
				$this->_sendResponse(400, CJSON::encode($json),'application/json');
				return;
			}

			if($record->save()){
				$record->password = "FILTERED";
				$this->_sendResponse(200, CJSON::encode(array('Code'=> '0','message'=>'merged successfully','model'=>$record)), 'application/json');
				return;
			}else{
				$this->_sendErrorResponse($record);
				return;
			}
		}else{


			$model = new Users;

			//ensure the model has this attribute
			foreach($_POST as $var=>$value){
				if($model->hasAttribute($var)){
					$model->$var = $value;
				}
				else{
					$msg = sprintf('Parameter %s is not allowed for model %s', $var,$_GET['model']);
					$this->_sendResponse(400,CJSON::encode(array('Message'=>$msg)),'application/json');
					return;
				}
			}

			//now lets do our hashing and creation of token
			$hash = password_hash($_POST['password'],PASSWORD_BCRYPT);
			$model->password = $hash;

			$token = bin2hex(openssl_random_pseudo_bytes(16));
			$model->api_token = $token;

			$now = new DateTime(null, new DateTimeZone('UTC'));
			$now->modify("+1 month");
			$model->token_expires = $now->format('Y/m/d H:i:s');

			if($model->save()){
				$model->password = "FILTERED";
				$json = array('Code'=>'0','Model'=>$model);
				$this->_sendResponse(200, CJSON::encode($json), 'application/json');
				return;
			}else{
				$this->_sendErrorResponse($model);
			}
		}
	}

	private function _listUsers(){
		if($this->_authenticate()){

			$criteria = new CDbCriteria;
			$criteria->select = 't.id, t.email, t.facebook_id, t.first_name, t.surname, t.gender, t.subscribed, t.height, t.current_day, t.current_weight, t.plan_type';
			$users = Users::model()->findAll($criteria);

			if(empty($users)){
				$msg =  sprintf('No items where found for model %s', $_GET['model']);
				$this->_sendResponse(200, CJSON::encode($msg), 'application/json');
			}else{
				$this->_sendResponse(200, CJSON::encode($users), 'application/json');
			}

		}
	}

	private function _getUser(){
		if($this->_authenticate()){
			$user = $this->_getUserPublicAttributes();

			if($user == null){
				$json = array('Code'=>1,'Message'=>'No record found!');
				$this->_sendResponse(404, CJSON::encode($json),'application/json');
			}else{
				$this->_sendResponse(200, CJSON::encode($user), 'application/json');
			}
		}else{
			$this->_sendResponse(403, sprintf("Bad Token, please try again"));
			return;
		}
	}

	private function _updateUser($_PUT){
		//need to reject any changes for password, api_token, token_expires -- maybe email & facebook_id (will restrict just to be sure)
		

		if($this->_authenticate()){

			$user = Users::model()->findByPk($_GET['id']);

			if($user == null){
				$json = array('Code'=>1,'Message'=>'Invalid user ID');
				$this->_sendResponse(404,CJSON::encode($json),'application/json');
			}

			foreach($_PUT as $var=>$value){
				// echo $var;
				if($user->hasAttribute($var))
					switch($var){//filter out params we cant set
						case 'password':
							$json = array('Code'=>2,'Message'=>'Cannot set password');
							$this->_sendResponse(400,CJSON::encode($json),'application/json');
							break;
						case 'api_token':
							$json = array('Code'=>2,'Message'=>'Cannot set api_token');
							$this->_sendResponse(400,CJSON::encode($json),'application/json');
							break;
						case 'token_expires':
							$json = array('Code'=>2,'Message'=>'Cannot set token expiration');
							$this->_sendResponse(400,CJSON::encode($json),'application/json');
							break;
						case 'email':
							$json = array('Code'=>2,'Message'=>'Cannot set email');
							$this->_sendResponse(400,CJSON::encode($json),'application/json');
							break;
						case 'facebook_id':
							$json = array('Code'=>2,'Message'=>'Cannot set facebook id');
							$this->_sendResponse(400,CJSON::encode($json),'application/json');
							break;
						default:
							$user->$var = $value;
							break;
					}
				else{
					$msg = sprintf('Parameter %s is not allowed for model %s', $var,$_GET['model']);
					$this->_sendResponse(400,CJSON::encode(array('Message'=>$msg)),'application/json');
					return;
				}
			}

			//now try to save the model
			if($user->save()){
				$jsonUser = $this->_getUserPublicAttributes();
				$this->_sendResponse(200, CJSON::encode($jsonUser),'application/json');
			}else{
				$this->_sendErrorResponse($user);
			}

		}
	}

	private function _updateDay($_PUT){
		if($this->_authenticate()){
			$user = Users::model()->findByPk($_GET['id']);

			if($user == null){
				$json = array('Code'=>1,'Message'=>'Invalid user ID');
				$this->_sendResponse(404,CJSON::encode($json),'application/json');
			}

			$user->current_day = $_PUT['current_day'];


			if($user->save()){
				$jsonUser = $this->_getUserPublicAttributes();
				$this->_sendResponse(200, CJSON::encode($jsonUser),'application/json');
			}else{
				$this->_sendErrorResponse($user);
			}
		}
	}

	private function _getUserPublicAttributes(){
		$criteria = new CDbCriteria;
		$criteria->select = 't.id, t.email, t.facebook_id, t.first_name, t.surname, t.gender, t.subscribed, t.height, t.current_day, t.current_weight, t.plan_type';
		$criteria->condition='id=:id';
		$criteria->params = array(':id'=>$_GET['id']);
		$user = Users::model()->findAll($criteria);
		return $user;
	}

	private function _sendErrorResponse($model){
		$json = array();
		$message = sprintf("Couldn't update model %s", $_GET['model']);
		$errors = array();
		$attributes = array();

        foreach($model->errors as $attribute=>$attr_errors) {

            $attr_errors_arr = array();
            foreach($attr_errors as $attr_error)
                $attr_errors_arr[] = $attr_error;

            $attributes[] = array('attribute' => $attribute, 'errors'=>$attr_errors_arr);
        }

        $json[] = array('message'=>$message, 'errors'=>$attributes);
    	$this->_sendResponse(500, CJSON::encode($json),'application/json' );
    	return;
	}

	private function _createUserWeightRelation(){
		if($this->_authenticate()){
			$model = new UsersWeightRelation;
			$model->user_id = $_GET['id'];

			$user = Users::model()->findByPk($_GET['id']);
			if($user == null){
				$json = array('Code'=>'1','Message'=>'No user found for this id');
				$this->_sendResponse(404, CJSON::encode($json), 'application/json');
			}

			$user->current_weight = $_POST['weight'];
			if(!$user->save()){
				$this->_sendErrorResponse($user);
				return;
			}

			foreach($_POST as $var=>$value){
				if($model->hasAttribute($var)){
					$model->$var = $value;
				}else{
					$msg = sprintf('Parameter %s is not allowed for model %s',$var,$_GET['model']);
					$this->_sendResponse(400,CJSON::encode(array('Message'=>$msg)),'application/json');
					return;
				}
			}

			if($model->save()){
				$this->_sendResponse(200, CJSON::encode($model), 'application/json');
				return;
			}else{
				$this->_sendErrorResponse($model);
			}
		}
	}

	private function _getWeightBetweenDates(){
		if($this->_authenticate() && $this->_validateDates($_GET['start_date'], $_GET['end_date'])){

			if(Users::model()->findByPk($_GET['id']) == null){
				$json = array('Code'=>1,'Message'=>'No user exists for this ID!');
				$this->_sendResponse(400, CJSON::encode($json), 'application/json');
				return;
			}

			$criteria = new CDbCriteria(array('order'=>'date DESC'));
			$criteria->condition= 'user_id='.$_GET['id'];
			$criteria->addBetweenCondition('date',$_GET['start_date'], $_GET['end_date']);
			$models = UsersWeightRelation::model()->findAll($criteria);

			if(empty($models)){
				$msg =  sprintf('No items where found for model %s', $_GET['model']);
				$this->_sendResponse(200, CJSON::encode($msg), 'application/json');
			}else{
				$this->_sendResponse(200, CJSON::encode($models), 'application/json');
			}

		}

	}

	private function _validateDates($start,$end){
		$sd = DateTime::createFromFormat('Y-m-d', $start);
		$ed = DateTime::createFromFormat('Y-m-d', $end);
		if(!$sd || $sd->format('Y-m-d') != $start){
			$json = array('Code'=>2,'Message'=>'Invalid start date');
			$this->_sendResponse(400, CJSON::encode($json), 'application/json');
			return false;
		}elseif(!$ed || $ed->format('Y-m-d') != $end){
			$json = array('Code'=>3,'Message'=>'Invalid end date');
			$this->_sendResponse(400, CJSON::encode($json), 'application/json');
			return false;
		}elseif($ed < $sd){
			$json = array('Code'=>4,'Message'=>'Start date after end date');
			$this->_sendResponse(400, CJSON::encode($json), 'application/json');
			return false;
		}else{
			return true;
		}
    	
	}

	private function _subscribeUser($_PUT){
		if($this->_authenticate()){
			$user = Users::model()->findByPk($_GET['id']);
			$user->subscribed = $_PUT['subscribed'];


			if($user->save()){
				$jsonUser = $user = $this->_getUserPublicAttributes();
				$this->_sendResponse(404, CJSON::encode($jsonUser),'application/json');
			}else{
				$this->_sendErrorResponse($user);
			}

		}
	}

	private function _createUserFavoriteRelation(){
		if($this->_authenticate()){
			$model = new UsersFavoritesRelation;
			$model->user_id = $_GET['id'];

			if(Users::model()->findByPk($_GET['id'])){
				$json = array('Code'=>1,'Message'=>'No user exists for this ID!');
				$this->_sendResponse(400, CJSON::encode($json), 'application/json');
				return;
			}

			foreach($_POST as $var=>$value){
				if($model->hasAttribute($var)){
					$model->$var = $value;
				}else{
					$msg = sprintf('Parameter %s is not allowed for model %s', $var,$_GET['model']);
					$this->_sendResponse(400,CJSON::encode(array('Message'=>$msg)),'application/json');
					return;
				}
			}
			
			if($model->save()){
				$this->_sendResponse(200, CJSON::encode($model), 'application/json');
				return;
			}else{
				$this->_sendErrorResponse($model);
			}

		}
	}

	private function _deleteFavorite(){
		if($this->_authenticate()){
			if(User::model()->findByPk($_GET['id']) == null){
				$json = array('Code'=>1,'Message'=>'No user exists for this ID!');
				$this->_sendResponse(400, CJSON::encode($json), 'application/json');
				return;
			}

			$favorite = UsersFavoritesRelation::model()->findByAttributes(array('id'=>$_GET['rel_id'],'user_id'=>$_GET['id']));

			if($favorite == null){
				$msg = sprintf('No items were found matching this criteria');
				$this->_sendResponse(404, CJSON::encode(array('Message'=>$msg)), 'application/json');
			}

			if($favorite->delete()){
				$this->_sendResponse(200, CJSON::encode($favorite),'application/json');
			}else{
				$this->_sendErrorResponse($model);
			}

			
		}
	}

	private function _getFavorites(){
		if($this->_authenticate()){
			$favorites = Users::model()->findByPk($_GET['id'])->usersFavoritesRelations;

			if(empty($favorites)){
				$msg =  sprintf('No items where found for model %s', $_GET['model']);
				$this->_sendResponse(404, CJSON::encode(array('Message'=>$msg)), 'application/json');
			}else{
				$this->_sendResponse(200, CJSON::encode($favorites),'application/json');
			}
			
		}
	}
}