<?php

class m140711_164116_update_weights extends CDbMigration
{
	public function up()
	{
		$this->alterColumn('users_weight_relation','date','date NOT NULL');
	}

	public function down()
	{
		echo "m140711_164116_update_weights does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}