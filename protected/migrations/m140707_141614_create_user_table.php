<?php

class m140707_141614_create_user_table extends CDbMigration
{
	public function up()
	{
		//Create the user table
		$this->createTable('users',array(
			'id'=>'pk',
			'email'=>'varchar(255) NOT NULL',
			'password'=>'char(60)',//not sure on length of this for bcrypt
			'facebook_id'=>'varchar(128)',//not sure what this is going to look like yet
			'first_name'=>'varchar(32) NOT NULL',
			'surname'=>'varchar(32) NOT NULL',
			'gender'=>'varchar(1) NOT NULL',
			'subscribed'=>'tinyint(1)',
			'height'=>'decimal(3,2)',
			'current_day'=>'int',
			'current_weight'=>'int',
			'plan_type'=>'int',//probably going to need a relation table for determining plan specifics
			'api_token'=>'varchar(255)',
			'token_expires'=>'datetime'
		),'ENGINE=InnoDB CHARSET=utf8');
	}

	public function down()
	{
		$this->dropTable('users');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}