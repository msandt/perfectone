<?php

class m140708_164217_user_weight_relation extends CDbMigration
{
	public function up()
	{
		//Create the user table
		$this->createTable('users_weight_relation',array(
			'id'=>'pk',
			'user_id'=>'int(11) NOT NULL',
			'weight'=>'int(11) NOT NULL',
			'date'=>'datetime NOT NULL'
		),'ENGINE=InnoDB CHARSET=utf8');

		$this->addForeignKey('fk1_users_weight_relation','users_weight_relation','user_id',
        					'users','id','CASCADE','NO ACTION');
	}

	public function down()
	{
		$this->dropTable('users_weight_relation');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}