<?php

class m140709_162113_user_favorites_relation extends CDbMigration
{
	public function up()
	{
		$this->createTable('users_favorites_relation',array(
			'id'=>'pk',
			'user_id'=>'int(11) NOT NULL',
			'program_id'=>'int(11) NOT NULL'
		),'ENGINE=InnoDB CHARSET=utf8');

		$this->addForeignKey('fk1_users_favorites_relation','users_favorites_relation','user_id','users','id','CASCADE','NO ACTION');

		//TODO -- implement program foreign key once programs are created
	}

	public function down()
	{
		$this->dropTable('users_favorites_relation');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}